class BST{
    Node head;
    
    BST(){ 
    	head=null;
	}
	
    Node insert(Node node, int val_){	
    	if(node==null){
    		node=new Node(val_);
    	}	
    	else if(node!=null){
    		if(val_<node.value){
    			node.left=insert(node.left, val_);
    		}
    		else if(val_>node.value){
    			node.right=insert(node.right, val_);
    		}
    	}
    	return node;
    }
    
    
   
    void insert(int val_){
    	head=insert(head, val_);
    }

    public static void main(String args[]){
		BST bst=new BST();
		bst.insert(8);
		bst.insert(12);
		bst.insert(10);
		bst.insert(9);
		bst.insert(11);
		bst.insert(12);
		bst.insert(14);
		bst.insert(13);
		bst.insert(15);
		bst.printInorder();
//		System.out.println(bst.left_max().value);
//		System.out.println(bst.right_min().value);
//		System.out.println(bst.find(5).value+" : bst.find(5).value");
//		System.out.println(bst.find_pre(5).value);
//		System.out.println(bst.find_par(15).value);
		System.out.println(bst.delete(9));
		bst.printInorder();
    } 
   
    public void printInorder(){
    	printInorder(head);
    	System.out.println();
    }  
    
    private void printInorder(Node node){
    	if(node==null){
    		return;
    	}
    	printInorder(node.left);
    	System.out.print(node.value+" ");
    	printInorder(node.right);
    }
    
    public Node left_max(){
    	return left_max(head.left);
    }
    private Node left_max(Node node){
    	Node current=node;
    	while(current.right!=null){
    		current=current.right;
    	}
    	return current;
    }
    
    public Node right_min(){
    	return right_min(head.right);
    }
    private Node right_min(Node node){
    	Node current=node;
    	while(current.left!=null){
    		current=current.left;
    	}
    	return current;
    }
    
    public Node find(int val_){
    	Node current=head;
    	while(val_ != current.value){
    		if(val_ < current.value){
    			if(current.left==null) return null; 
    			current=current.left;
    		}
    		else if(val_ > current.value){
    			if(current.right==null) return null;
    			current=current.right;
    		}
    	}
    	return current;
    }
    
    public Node find_pre(int val_){
    	
//    	return find_pre(val_, find(val_));
		if(find(val_).left==null && find(val_).right==null){
			return null;
		}
		else if(find(val_).left==null){
			return find_pre(right_min(find(val_).right), head);
		}
		else{
			return find_pre(left_max(find(val_).left), head);
		}
    }
    private Node find_pre(Node min_node, Node head_node){
    	Node current=min_node;
    	Node temp=head_node;
    	
    	if(head_node.left==min_node || head_node.right==min_node){
    	
    		return head_node; 
    	}
    	else if(temp.value > current.value){
    		return find_pre(current, temp.left);
    	}
    	else 
    		return find_pre(current, temp.right);
    
    }
    
    public Node find_suc(int val_){
    	if(find(val_).left==null && find(val_).right==null){
			return null;
		}
		else if(find(val_).left==null){
			return right_min(find(val_).right);
		}
		else{
			return left_max(find(val_).left);
		}
    }
    
    public Node find_par(int val_){
    	return find_par(val_, head);
    }
    
    public Node find_par(int val_, Node node){
    	Node current=find(val_);
    	Node temp= node;
    	
    	if(current==node){
    		
    		return null;
    	}
	 	
    	else if(temp.right==current || temp.left==current){
    		return temp;
    	}
    	else if(temp.value < current.value){
    		return find_par(val_, temp.right);
    	}
    	else 
    		return find_par(val_, temp.left);
    }
    
    public Node delete(int val_){
    	Node node	=	find(val_);
    	Node node_1	= 	find_pre(val_);
    	Node node_2 =	find_suc(val_);
    		
    		if(node.left==null && node.right==null) {
    			if(find_par(val_)!=null){
    				if(find_par(val_).left==node){find_par(val_).left=null;}
    				else if(find_par(val_).right==node){find_par(val_).right=null;}
    			}
    			node=null;
    		}
    		
    		else if(node.left==null){
    			node_1.left=node_2.right;
    			node.value=node_2.value;
    			
    		}
   			
    		else {
    			node_1.right=node_2.left;
    			node.value=node_2.value;
    			
    		}   
    		return node;
    }
    
    
    
    
    

}








































